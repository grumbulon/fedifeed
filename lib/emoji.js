import axios from "axios";
import fs from "fs";

//Grab emote list so when rendering 
export default function getEmojiJson(url) {
    let emojiUrl = "https://" + url + "/api/v1/custom_emojis";
    axios
        .get(emojiUrl)
        .then(res => {
            console.log(`statusCode: ${res.status}`);
            const emojiJson = res.data;
            console.log(emojiJson);
        });
}
getEmojiJson("freecumextremist.com");